From Cupak2019

Slovak Household Finance and Consumption Survey (HFCS) data collected by the National Bank of
Slovakia in 2014. We utilise recent Household Finance and Consumption Survey microdata to report first
causal effects of financial literacy on voluntary private pension schemes participation
for a Central and Eastern European (CEE) country, namely Slovakia. We instrument the financial literacy
of survey respondents with interviewers’ assessments of respondents’ abilities to understand financial questions
in the survey and with interviewers’ assessments of respondents’ abilities to translate monetary values from Slovak crowns to recently introduced euros. Particularly, the second instrument is relevant for the Slovak case, as
many households acquired their assets prior to 2009, when the Euro was launched as a new currency and still
tend to express values in the former currency. 
In the survey, each household represented by the reference person is asked a set of questions on financial literacy.
Inspired by the previous literature (e.g. Lusardi and Mitchell 2014), questions are formed in order to discover
the ability of respondents to understand fundamental concepts in personal finance including interest rates, inflation, riskiness and diversification of portfolios. The questions regarding interest rates and inflation indicate the
level of respondents’ understanding of fundamental economic concepts for saving decisions and basic financial
numeracy (Lusardi and Mitchell 2011c). Questions focused on the portfolio diversification and risk help to evaluate respondents’ knowledge on how financial assets work and if there are familiar with the concept of risk diversification that are important factors of an informed investment decision (Lusardi and Mitchell 2011c). These
concepts represent fundamental financial knowledge for competent retirement saving decisions (Lusardi and
Mitchell 2017). The full list of financial literacy questions asked in the Slovak HFCS is presented in Appendix C.
Following previous studies, we create the first measure of financial literacy as a sum of binary variables taking
the value of 1 if the particular financial literacy question is answered correctly and 0 otherwise. Our financial
literacy index ranges between 0 and 4 for each individual. As an alternative measure of financial literacy, we
create a dummy variable taking value 1 if all financial literacy questions are answered correctly and 0 otherwise.
These two measures of financial literacy are the most commonly used in the extant empirical literature)
